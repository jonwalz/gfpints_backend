package models

import "github.com/satori/go.uuid"

type User struct {
	ID       uuid.UUID `db:"id"`
	Username string    `db:"username"`
}
