package actions

import (
	"github.com/gfpints_api/models"
	"github.com/gobuffalo/buffalo"
	"github.com/satori/go.uuid"
)

var db = make(map[uuid.UUID]models.User)

type UserResource struct{}

// List all the users
func (ur UserResource) List(c buffalo.Context) error {
	return c.Render(200, r.JSON(db))
}

// Create User
func (ur UserResource) Create(c buffalo.Context) error {
	userID, _ := uuid.NewV4()
	username := c.Request().FormValue("Username")

	user := &models.User{
		ID:       userID,
		Username: username,
	}
	db[user.ID] = *user

	return c.Render(201, r.JSON(user))
}

func (ur UserResource) Show(c buffalo.Context) error {
	id, err := uuid.FromString(c.Param("id"))
	if err != nil {
		return c.Render(500, r.String("id is not uuid v4"))
	}

	user, ok := db[id]
	if ok {
		return c.Render(200, r.JSON(user))
	}

	return c.Render(404, r.String("user not found"))
}
